'use strict';

const express = require('express');
const multer = require('multer');

const app = express();
const parts = multer();
let server = null;

app.use(express.static('public'));

app.post('/api/metadata', parts.single('upfile'), (request, response) => {
  if (!request.file) {
    return response.json({error: 'no file was uploaded'});
  }
  return response.json({
    field: request.file.fieldname,
    name: request.file.originalname,
    encoding: request.file.encoding,
    type: request.file.mimetype,
    size: request.file.size
  });
});

async function clean() {
  await Promise.all([
    new Promise(resolve => server.close(resolve))
  ]);
}

async function main() {
  server = app.listen(8080);
}

process.on('SIGINT', clean);
process.on('SIGTERM', clean);

main();
